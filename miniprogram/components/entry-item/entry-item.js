Component({
  behaviors: [],
  properties: {
    title: {
      type: String,
      value: ""
    },
    meta: {
      type: String,
      value: ""
    },
    flag: {
      type: Boolean,
      value: false
    }
    
  },
  data: {},
  lifetimes: {
    created() {

    },
    attached() {

    },
    moved() {

    },
    detached() {

    },
  },
  methods: {},
});
