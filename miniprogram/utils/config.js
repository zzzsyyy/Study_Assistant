const pages = {
    'index': '/pages/index/index',
    'profile': '/pages/profile/profile',
    'about': '/pages/profile/about/about',
    'login': '/pages/profile/login/login',
    'collection': '/pages/collection/collection',
    'editEntry': '/pages/editEntry/editEntry',
    'previewEdit': '/pages/editEntry/previewEdit/previewEdit',
    'entryDetail': '/pages/entryDetail/entryDetail',
    'searchEntry': '/pages/searchEntry/searchEntry',
    'myEdit': '/pages/myEdit/myEdit'
};

export default pages;